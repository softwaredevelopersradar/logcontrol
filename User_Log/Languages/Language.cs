﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace User_Log
{
    public partial class UserControlLog : UserControl
    {
        public void SetLanguge(Languages newLanguage)
        {
            try
            {
                ResourceDictionary dict = new ResourceDictionary();
                switch (newLanguage)
                {
                    case Languages.EN:
                        dict.Source = new Uri("/User_Log;component/Languages/StringResource.EN.xaml",
                            UriKind.Relative);
                        break;
                    case Languages.RU:
                        dict.Source = new Uri("/User_Log;component/Languages/StringResource.RU.xaml",
                            UriKind.Relative);
                        break;
                    case Languages.AZ:
                        dict.Source = new Uri("/User_Log;component/Languages/StringResource.AZ.xaml",
                            UriKind.Relative);
                        break;
                    default:
                        dict.Source = new Uri("/User_Log;component/Languages/StringResource.EN.xaml",
                            UriKind.Relative);
                        break;
                }
                Resources.MergedDictionaries.Add(dict);
            }
            catch (Exception)
            {
                //TODO
            }
        }

    }

    public enum Languages : byte
    {
        [Description("Русский")] RU = 0,
        [Description("English")] EN = 1,
        [Description("Azərbaycan")] AZ = 2,
    }
}
