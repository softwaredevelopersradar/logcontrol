﻿using System;
using System.IO;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Media;

namespace User_Log
{
    /// <summary>
    ///     Логика взаимодействия для UserControl1.xaml
    /// </summary>
    public partial class UserControlLog : UserControl
    {
        private StreamWriter _fstream;
        private readonly FlowDocument _myFlowDoc;
        private readonly Paragraph _myParagraph;
        private double fonteSize = 12;
        private int i;
        private string pathToFile = "";


        public UserControlLog()
        {
            InitializeComponent();

            SetLanguge(Languages.EN);

            _myFlowDoc = new FlowDocument();
            _myParagraph = new Paragraph();

            _myFlowDoc.Blocks.Add(_myParagraph);
            richTextBox.Document = _myFlowDoc;

            _myFlowDoc.PageWidth = richTextBox.Width;
            _myFlowDoc.IsOptimalParagraphEnabled = true;
            _myFlowDoc.IsHyphenationEnabled = true;
            fonteSize = richTextBox.FontSize;
        }

        public bool DisplayTime { get; set; } = true; //отображать ли время при выводе
        public string StartLine { get; set; } = ""; //текст выводящийся после отчищения поля
        public Brush StartLineColor { get; set; } = Brushes.LightGray; // цвет StartLine
        public Brush TimeColor { get; set; } = Brushes.Aquamarine; // цвет выводимого времени
        public bool ShowAllByte { get; private set; }
        public int AmountOfClearedLines { get; set; } = 30;
        public int ValueForClear { get; set; } = 60;

        public double UserFontSize
        {
            get => fonteSize;
            set
            {
                if (fonteSize == value) return;
                fonteSize = value;
            }
        } // размер шрифта

        public Visibility VisibilityBytesCheckBox
        {
            get => allByte.Visibility;
            set => allByte.Visibility = value;
        }


        /// <summary>
        ///     Имя текстового файла, в который сохраняется лог
        /// </summary>
        public string NameOfTextFile { get; set; } = "AllOutput.txt";

        /// <summary>
        ///     Путь к текстовому файлу, в который сохраняется лог
        /// </summary>
        public string PathToTextFile
        {
            get => pathToFile;

            set
            {
                if (value != null)
                {
                    if (NameOfTextFile.Contains(".txt"))
                        pathToFile = $@"{value}\{NameOfTextFile}";
                    else
                        pathToFile = $@"{value}\{NameOfTextFile}.txt";

                    try
                    {
                        _fstream = new StreamWriter(pathToFile, false, Encoding.Default);
                    }
                    catch (FileNotFoundException)
                    {
                        MessageBox.Show("Invalid path to text file (Log): " + value);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Error related with file (Log): " + ex);
                    }
                }
                else
                {
                }
            }
        }

        public void CloseLogFile()
        {
            _fstream?.Close();
        }

        public void AddTextToFile(string text)
        {
            _fstream?.Write(text);
        }



        public void AddTextToLog(string text, Brush LogBrush, bool isMarkedAsByte)
        {
            if (isMarkedAsByte && ShowAllByte == false)
                return;

            string str = "";
            if (DisplayTime)
            {
                var run_time = CreateRun(DateTime.Now.ToLongTimeString(), TimeColor);
                _myParagraph.Inlines.Add(run_time);
                str = run_time.Text;
            }

            var run = CreateRun(" " + text + "\n", LogBrush);
            str += run.Text;
            _myParagraph.Inlines.Add(run);

            richTextBox.ScrollToEnd();

            if (!isMarkedAsByte)
                _fstream?.Write(str);
        

            if (i > ValueForClear)
            {
                OverfolowClear();
                i = i - AmountOfClearedLines;
            }
            i++;
        }

        public void AddTextToLog(string text, Brush LogBrush)
        {
           AddTextToLog(text, LogBrush, false);
        }

        public void AddTextToLog(string text)
        {
           AddTextToLog(text, Brushes.White);
        }

        public void ClearLog()
        {
            _myParagraph.Inlines.Clear();
            i = 0;
            if (StartLine != "")
                AddTextToLog(StartLine, StartLineColor);
        }


        private Run CreateRun(string text, Brush color)
        {
            var run = new Run(text);
            run.FontFamily = new FontFamily("Times New Roman");
            run.FontSize = fonteSize;
            run.Foreground = color;
            
            return run;
        }


        private void OverfolowClear()
        {
            richTextBox.Document.Blocks.Clear();
            _myFlowDoc.Blocks.Clear();

            var n = AmountOfClearedLines;
            if (DisplayTime)
                n *= 2;

            for (var j = 0; j < n; j++) 
                _myParagraph.Inlines.Remove(_myParagraph.Inlines.FirstInline);

            _myFlowDoc.Blocks.Add(_myParagraph);
            richTextBox.Document = _myFlowDoc;
            if (_fstream != null)
            {
                _fstream.Close();
                _fstream = new StreamWriter(pathToFile, true, Encoding.Default);
            }
        }


        private void Button_Click(object sender, RoutedEventArgs e)
        {
            ClearLog();
        }

        private void ShowAllByte_Checked(object sender, RoutedEventArgs e)
        {
            ShowAllByte = true;
        }

        private void AllByte_Unchecked(object sender, RoutedEventArgs e)
        {
            ShowAllByte = false;
        }
    }
}