﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using User_Log;

namespace TestApp
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            rr.PathToTextFile = Environment.CurrentDirectory;
        }
        private Brush Color1 = null;

        private void TextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                
                if (Color1 != null)
                    rr.AddTextToLog(TB.Text.Trim('\n'), Color1, true);
                else 
                   rr.AddTextToLog(TB.Text.Trim('\n'), Brushes.White,true);

              //  for (int i = 0; i < 350; i++)
              //    rr.AddTextToLog("Massage number:  " + Convert.ToString(i));

            }
            l.Content = "Show all byte: " + Convert.ToString(rr.ShowAllByte);
        }

        private void CB_Checked(object sender, RoutedEventArgs e)
        {
            rr.DisplayTime = true;
        }

        private void CB_Unchecked(object sender, RoutedEventArgs e)
        {
            rr.DisplayTime = false;
        }

        private void CB1_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            switch (CB1.SelectedIndex)
            {
                case 0:
                    Color1 = Brushes.Green;
                    break;
                case 1:
                    Color1 = Brushes.Red;
                    break;
                case 2:
                    Color1 = Brushes.Blue;
                    break;
                case 3:
                    Color1 = Brushes.LightGray;
                    break;
                case 4:
                    Color1 = Brushes.Black;
                    break;
            }
        }

        private void CB2_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            switch (CB2.SelectedIndex)
            {
                case 0:
                    rr.TimeColor = Brushes.Green;
                    break;
                case 1:
                    rr.TimeColor = Brushes.Red;
                    break;
                case 2:
                    rr.TimeColor = Brushes.Blue;
                    break;
                case 3:
                    rr.TimeColor = Brushes.LightGray;
                    break;
                case 4:
                    rr.TimeColor = Brushes.Black;
                    break;
            }
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            rr.CloseLogFile();
        }
    }      
}
